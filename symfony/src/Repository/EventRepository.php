<?php

namespace App\Repository;

use App\Entity\Event;
use App\Entity\Coordinates;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

use Psr\Log\LoggerInterface;

/**
 * @method Event|null find($id, $lockMode = null, $lockVersion = null)
 * @method Event|null findOneBy(array $criteria, array $orderBy = null)
 * @method Event[]    findAll()
 * @method Event[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EventRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry, LoggerInterface $logger)
    {
        parent::__construct($registry, Event::class);
    }

    public function getEventsByKeywords($keyword){

        return $this->createQueryBuilder('e')
            ->andWhere('e.title LIKE :key')
            ->setParameter('key' , '%'.$keyword.'%')
            ->getQuery()
            ->getResult()
        ;
    }

    public function getNothing(){
        return $this->createQueryBuilder('e')
            ->andWhere('1=0')
            ->getQuery()
            ->getResult()
        ;
    }

    public function getEventsByDate($date, $dateFin){

        return $this->createQueryBuilder('e')
            //->andWhere('e.date = :date')
            ->andWhere('e.date BETWEEN :date AND :dateFin')
            ->setParameter('date', $date)
            ->setParameter('dateFin', $dateFin)
            ->getQuery()
            ->getResult()
        ;
    }

    public function getEventsByLocalisation($localisation){
        
        return $this->createQueryBuilder('e')
            ->join('e.coordinates', 'c')
            ->where('c.postalCode = :localisation')
            ->setParameter('localisation', $localisation)
            ->getQuery()
            ->getResult()
        ;
        
    }

    // /**
    //  * @return Event[] Returns an array of Event objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Event
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
