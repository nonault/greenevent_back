<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\ItemRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use App\Controller\BuyItemController;

/**
 * @ApiResource(
 *      itemOperations= {
 *          "get"= {
 *              "formats"= {"json"},
 *              "filters" = {},
 *              "pagination_enabled" = false,
 *              "openapi_context"= {
 *                  "parameters" = {},
 *                  "summary" = "Permet de récupérer un item avec son id.",
 *                  "description" = ""
 *              },
 *          },
 *          "buy" = {
 *              "method" = "PUT",
 *              "path" = "/item/{id}/add-user",
 *              "controller" = BuyItemController::class,
 *              "denormalization_context" = {"groups"={"items:write"}},
 *              "validate"=false,
 *              "write" = true,
 *              "formats"= {"json"},
 *              "pagination_enabled" = false,
 *              "openapi_context"= {
 *                  "parameters" = {},
 *                  "summary" = "Permet de modifier un item avec son id.",
 *                  "description" = ""
 *              },
 *          },
 *          "delete" = {
 *              "formats"= {"json"},
 *              "pagination_enabled" = false,
 *              "openapi_context"= {
 *                  "parameters" = {},
 *                  "summary" = "Permet de supprimer un item avec son id.",
 *                  "description" = ""
 *              },
 *          }
 * },
 *      collectionOperations={
 *          "post" = {
 *              "formats"= {"json"},
 *              "pagination_enabled" = false,
 *              "openapi_context"= {
 *                  "parameters" = {},
 *                  "summary" = "Permet de poster un item.",
 *                  "description" = ""
 *              },
 *          },
 *          "get" = {
 *              "formats"= {"json"},
 *              "pagination_enabled" = false,
 *              "openapi_context"= {
 *                  "parameters" = {},
 *                  "summary" = "Permet de récupérer tous les items.",
 *                  "description" = ""
 *              },
 *          }
 *      },
 * )
 * @ORM\Entity(repositoryClass=ItemRepository::class)
 */

class Item
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"heavy:read","lazy:read"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"heavy:read"})
     */
    private $name;

    /**
     * @ORM\Column(type="integer")
     * @Groups({"heavy:read"})
     */
    private $quantity;

    /**
     * @ORM\Column(type="integer")
     * @Groups({"heavy:read"})
     */
    private $cost;

    /**
     * @ORM\ManyToOne(targetEntity=Association::class, inversedBy="items")
     * @ORM\JoinColumn(nullable=false)
     */
    private $association;

    /**
     * @ORM\ManyToMany(targetEntity=User::class, mappedBy="items")
     * @Groups({"items:write"})
     */
    private $users;



  





    public function __construct()
    {
        $this->users = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getQuantity(): ?int
    {
        return $this->quantity;
    }

    public function setQuantity(int $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }

    public function getCost(): ?int
    {
        return $this->cost;
    }

    public function setCost(int $cost): self
    {
        $this->cost = $cost;

        return $this;
    }

    public function getAssociation(): ?Association
    {
        return $this->association;
    }

    public function setAssociation(?Association $association): self
    {
        $this->association = $association;

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function addUser(User $user): self
    {
        if (!$this->users->contains($user)) {
       
            $this->users[] = $user;
            $user->addItem($this);
        }

        return $this;
    }

    public function removeUser(User $user): self
    {
        if ($this->users->removeElement($user)) {
            $user->removeItem($this);
        }

        return $this;
    }


  
}
