<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\PostRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(
 *
 *      itemOperations= {
 *          "get" = {
 *              "formats"= {"json"},
 *              "pagination_enabled" = false,
 *              "openapi_context"= {
 *                  "parameters" = {},
 *                  "summary" = "Permet de récupérer un post avec son id.",
 *                  "description" = ""
 *              },
 *          },
 *          "delete" = {
 *              "formats"= {"json"},
 *              "pagination_enabled" = false,
 *              "openapi_context"= {
 *                  "parameters" = {},
 *                  "summary" = "Permet de supprimer un post avec son id.",
 *                  "description" = ""
 *              },
 *          }
 *      },
 *      collectionOperations={
 *          "post" = {
 *              "formats"= {"json"},
 *              "pagination_enabled" = false,
 *              "openapi_context"= {
 *                  "parameters" = {},
 *                  "summary" = "Permet d'ajouter un post.",
 *                  "description" = ""
 *              },
 *          },
 *          "get" = {
 *              "formats"= {"json"},
 *              "pagination_enabled" = false,
 *              "openapi_context"= {
 *                  "parameters" = {},
 *                  "summary" = "Permet de récupérer tous les posts..",
 *                  "description" = ""
 *              },
 *          }
 *      },
 * )
 * @ORM\Entity(repositoryClass=PostRepository::class)
 */

class Post
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"heavy:read","lazy:read"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"heavy:read"})
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"heavy:read"})
     */
    private $description;

    /**
     * @ORM\Column(type="text")
     * @Groups({"heavy:read"})
     */
    private $contenu;

    /**
     * @ORM\ManyToOne(targetEntity=Association::class, inversedBy="posts")
     * @ORM\JoinColumn(nullable=false)
     */
    private $association;

    /**
     * @ORM\ManyToMany(targetEntity=User::class, inversedBy="posts")
     * @Groups({"heavy:read"})
     */
    private $likes;

    public function __construct()
    {
        $this->likes = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }


    public function getContenu(): ?string
    {
        return $this->contenu;
    }

    public function setContenu(string $contenu): self
    {
        $this->contenu = $contenu;

        return $this;
    }

    public function getAssociation(): ?Association
    {
        return $this->association;
    }

    public function setAssociation(?Association $association): self
    {
        $this->association = $association;

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getLikes(): Collection
    {
        return $this->likes;
    }

    public function addLike(User $like): self
    {
        if (!$this->likes->contains($like)) {
            $this->likes[] = $like;
        }

        return $this;
    }

    public function removeLike(User $like): self
    {
        $this->likes->removeElement($like);

        return $this;
    }
}
