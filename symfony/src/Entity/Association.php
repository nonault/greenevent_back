<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\AssociationRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use ApiPlatform\Core\Annotation\ApiProperty;


/**
 * @ApiResource(
 *  denormalizationContext={"groups"={"cheese_listing:write"}},

 * itemOperations= {
 *     "lazy" = {
 *         "method" = "GET",
 *          "path" = "/association/{id}",
 *          "normalization_context" = {"groups"={"lazy:read"}},
 *          "formats"= {"json"},
 *          "filters" = {},
 *          "pagination_enabled" = false,
 *          "openapi_context"= {
 *              "summary" = "Permet de récupérer une association à partir de son id.",
 *              "parameters" = {},
 *              "description" = "Cette route vous permet de récupérer toutes les données d'une association à partir de son id. Les données suivantes sont renvoyées: 
 * id,
 * name,
 * siret,
 * email,
 * phoneNumber,
 * identifiants des evenements dans un tableau,
 * identifiants des administrateurs dans un tableau,
 * identifiant des coordonnées,
 * identifiants des items dans un tableau."
 *          }
 *      },
 *      "members" = {
 *         "method" = "GET",
 *          "path" = "/association/{id}/admins",
 *          "normalization_context" = {"groups"={"admins:read"}},
 *          "formats"= {"json"},
 *          "filters" = {},
 *          "pagination_enabled" = false,
 *          "openapi_context"= {
 *              "parameters" = {},
 *              "summary" = "Permet de récupérer les données de tous les administateurs d'une association à partir de son id.",
 *              "description"= "Cette route vous permet de recupérer tous les adminisateurs d'une association les données suivantes sont renvoyées pour chaque administateur: 
 * firstname, 
 * lastname, 
 * email, 
 * points, 
 * id."
 *          }
 *      },
 *
 *     "heavy"={
 *         "method" = "GET",
 *         "path" = "/association/{id}/heavy",
 *         "normalization_context" = {"groups"={"admins:read", "heavy:read"}},
 *         "formats"= {"json"},
 *         "filters" = {},
 *         "pagination_enabled" = false,
 *          "openapi_context"= {
 *              "parameters" = {},
 *              "summary" = "Permet de récupérer les données detaillées d'une association à partir de son id.",
 *              "description" = "Cette route vous permet de récupérer toutes les données d'une association à partir de son id. Les données suivantes sont renvoyées: 
 * id,
 * name,
 * siret,
 * email,
 * phoneNumber,
 * évenements (id, title, date, quota, cost, description) dans un tableau,
 * administrateurs (id, lastname, firstname, email, points) dans un tableau,
 * coordonnées (id, latitude, longitude, address, category, name, postalCode),
 * items (id, name, quantity, cost) dans un tableau."}
 *     },
 *     "events"= {
 *          "method" = "GET",
 *          "path" = "/association/{id}/events/lazy",
 *          "normalization_context" = {"groups"={"eventslazy:read"}},
 *          "formats"= {"json"},
 *          "filters" = {},
 *          "pagination_enabled" = false,
 *          "openapi_context"= {
 *              "parameters" = {},
 *              "summary" = "Permet de récupérer les données sans détail d'une association à partir de son id.",
 *              "description" = "Cette route vous permet de récupérer les données d'une association à partir de son id. Les données suivantes sont renvoyées: 
 * id,
 * name,
 * siret,
 * email,
 * phoneNumber,
 * évenements (id) dans un tableau,
 * administrateurs (id) dans un tableau,
 * coordonnées (id),
 * items (id) dans un tableau."}
 *        
 *      },
 *      "eventsheavy" = {
 *          "method"= "GET",
 *          "path" = "/association/{id}/events/heavy",
 *          "normalization_context" = {"groups"={"eventsheavy:read"}},
 *          "formats"= {"json"},
 *          "filters" = {},
 *          "pagination_enabled" = false,
 *          "openapi_context"= {
 *              "parameters" = {},
 *              "summary" = "Permet de récupérer les evenements détaillés d'une association à partir de son id.",
 *              "description" = "Cette route vous permet de récupérer les évenements détaillés d'une association à partir de son id. Les données suivantes sont renvoyées: 
 * event (id,title,date,quota,description,cost,participants,coordonnées)"},
 *          
 *      },
 *     "delete" = {
 *          "formats"= {"json"},
 *          "filters" = {},
 *          "pagination_enabled" = false,
 *          "openapi_context"= {
 *              "parameters" = {},
 *              "summary" = "Permet de supprimer une association à partir de son id.",
 *              "description" = "Cette route vous permet de supprimer une association à partir de son id"
 *          },
 *      },
 *     "putasso" = {
 *          "method"= "PUT",
 *          "path" = "/association/{id}/update",
 *          "denormalization_context" = {"groups"={"updateasso:write"}},
 *          "validate"=false,
 *          "write" = true,
 *          "formats"= {"json"},
 *          "filters" = {},
 *          "pagination_enabled" = false,
 *          "openapi_context"= {
 *              "requestBody" = {
 *                  "content" = {
 *                       "application/json" = {
 *                           "schema"  = {
 *                               "type" = "object",
 *                               "properties" =
 *                                   {
 *                                       "name" = {"type" = "string"},
 *                                       "email" = {"type" = "string"},
 *                                       "phoneNumber" = {"type" = "string"},
 *                                       "image" = {"type" = "string"},
 *                                   },
 *                           },
 *                           "example" = {
 *                               "name" = "Green Event",
 *                               "email" = "greenevent@gmail.com",
 *                               "phoneNumber" = "0678767876",
 *                               "image" = "url",
 *                           },
 *                       },
 *                   },
 *               },
 *              "summary" = "Permet de modifier une association à partir de son id.",
 *              "description" = "Cette route vous permet de modifier une association à partir de son id. Les champs suivants peuvent être modifiés :
 * name,
 * email,
 * phoneNumber,
 * image"},
 *      },
 * },
 *  collectionOperations={
 *      "getheavy" = {
 *          "method"="GET",
 *          "normalization_context" = {"groups"={"heavy:read"}},
 *          "formats"= {"json"},
 *          "filters" = {},
 *          "pagination_enabled" = false,
 *          "openapi_context"= {
 *              "parameters" = {},
 *              "summary" = "Permet de récupérer toutes les données sans détail de toutes les associations.",
 *              "description" = "Cette route vous permet de récupérer toutes les données sans détail de toutes les associations. Les données suivantes sont renvoyées: 
 * id,
 * name,
 * siret,
 * email,
 * phoneNumber,
 * évenements (id) dans un tableau,
 * administrateurs (id) dans un tableau,
 * coordonnées (id),
 * items (id) dans un tableau."}
 *      },
 *      "post" = {
 *          "formats"= {"json"},
 *          "denormalization_context" = {"groups"={"createasso:write"}},
 *          "filters" = {},
 *          "pagination_enabled" = false,
 *          "openapi_context"= {
 *              "parameters" = {},
 *              "summary" = "Permet de créer une association.",
 *              "description" = "Cette requête permet de poster une nouvelle association."
 *          }
 *      },
 * },
 * )
 * @ORM\Entity(repositoryClass=AssociationRepository::class)
 * @ORM\Table(name="association",uniqueConstraints={@ORM\UniqueConstraint(name="siret_idx", columns={"siret"})})
 */

class Association
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @ApiProperty(identifier=true)
     * @Groups({"admins:read","heavy:read","lazy:read","coord:read"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"heavy:read","lazy:read","createasso:write","updateasso:write"})
     */
    private $name;

    /**
     * @ORM\Column(type="integer")
     * @Groups({"heavy:read","lazy:read","createasso:write"})
     */
    private $siret;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"heavy:read","lazy:read","createasso:write","updateasso:write"})
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"heavy:read","lazy:read","createasso:write","updateasso:write"})
     */
    private $phoneNumber;

    /**
     * @ORM\Column(type="text")
     * @Groups({"heavy:read","lazy:read", "eventslazy:read","updateasso:write","createasso:write"})
     */
    private $image;

    /**
     * @ORM\Column(type="text")
     * @Groups({"heavy:read","lazy:read", "eventslazy:read","createasso:write"})
     */
    private $description;

    /**
     * @ORM\OneToMany(targetEntity=Event::class, mappedBy="association", orphanRemoval=true)
     * @Groups({"heavy:read","lazy:read", "eventslazy:read", "eventsheavy:read"})
     */
    private $events;

    /**
     * @ORM\OneToMany(targetEntity=User::class, mappedBy="association")
     * @Groups({"admins:read","lazy:read","admins:write","createasso:write"})
     */
    private $admins;

    /**
     * @ORM\ManyToOne(targetEntity=Coordinates::class, inversedBy="association",cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"heavy:read","lazy:read","createasso:write"})
     */
    private $coordinates;

    /**
     * @ORM\OneToMany(targetEntity=Post::class, mappedBy="association", orphanRemoval=true)
     */
    private $posts;

    /**
     * @ORM\OneToMany(targetEntity=Item::class, mappedBy="association", orphanRemoval=true)
     * 
     */
    private $items;

    public function __construct()
    {
        $this->events = new ArrayCollection();
        $this->admins = new ArrayCollection();
        $this->posts = new ArrayCollection();
        $this->items = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSiret(): ?int
    {
        return $this->siret;
    }

    public function setSiret(int $siret): self
    {
        $this->siret = $siret;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getPhoneNumber(): ?string
    {
        return $this->phoneNumber;
    }

    public function setPhoneNumber(string $phoneNumber): self
    {
        $this->phoneNumber = $phoneNumber;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(string $image): self
    {
        $this->image = $image;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return Collection|Event[]
     */
    public function getEvents(): Collection
    {
        return $this->events;
    }

    public function addEvent(Event $event): self
    {
        if (!$this->events->contains($event)) {
            $this->events[] = $event;
            $event->setAssociation($this);
        }

        return $this;
    }

    public function removeEvent(Event $event): self
    {
        if ($this->events->removeElement($event)) {
            // set the owning side to null (unless already changed)
            if ($event->getAssociation() === $this) {
                $event->setAssociation(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getAdmins(): Collection
    {
        return $this->admins;
    }

    public function addAdmin(User $admin): self
    {
        if (!$this->admins->contains($admin)) {
            $this->admins[] = $admin;
            $admin->setAssociation($this);
        }

        return $this;
    }

    public function removeAdmin(User $admin): self
    {
        if ($this->admins->removeElement($admin)) {
            // set the owning side to null (unless already changed)
            if ($admin->getAssociation() === $this) {
                $admin->setAssociation(null);
            }
        }

        return $this;
    }

    public function getCoordinates(): ?Coordinates
    {
        return $this->coordinates;
    }

    public function setCoordinates(?Coordinates $coordinates): self
    {
        $this->coordinates = $coordinates;

        return $this;
    }

    /**
     * @return Collection|Post[]
     */
    public function getPosts(): Collection
    {
        return $this->posts;
    }

    public function addPost(Post $post): self
    {
        if (!$this->posts->contains($post)) {
            $this->posts[] = $post;
            $post->setAssociation($this);
        }

        return $this;
    }

    public function removePost(Post $post): self
    {
        if ($this->posts->removeElement($post)) {
            // set the owning side to null (unless already changed)
            if ($post->getAssociation() === $this) {
                $post->setAssociation(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Item[]
     */
    public function getItems(): Collection
    {
        return $this->items;
    }

    public function addItem(Item $item): self
    {
        if (!$this->items->contains($item)) {
            $this->items[] = $item;
            $item->setAssociation($this);
        }

        return $this;
    }

    public function removeItem(Item $item): self
    {
        if ($this->items->removeElement($item)) {
            // set the owning side to null (unless already changed)
            if ($item->getAssociation() === $this) {
                $item->setAssociation(null);
            }
        }

        return $this;
    }
}
