<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\EventRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Controller\GetEventWithKeywordsController;
use App\Controller\GetEventsWithDate;
use App\Controller\GetEventsWithLocalisation;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(
 *  itemOperations= {
 *          "get"= {
 *              "method" = "GET",
 *              "path" = "/event/{id}",
 *              "formats"= {"json"},
 *              "normalization_context" = {"groups"={"eventheavy:read","eventcoord:read"}},
 *              "filters" = {},
 *              "pagination_enabled" = false,
 *              "openapi_context"= {
 *                  "parameters" = {},
 *                  "summary" = "Permet de récupérer un évenement à partir de son id.",
 *                  "description" = ""
 *              },
 *          },
 *          "delete"= {
 *              "formats"= {"json"},
 *              "filters" = {},
 *              "pagination_enabled" = false,
 *              "openapi_context"= {
 *                  "parameters" = {},
 *                  "summary" = "Permet de supprimer un evenement à partir de son id.",
 *                  "description" = ""
 *              },
 *          },
 *          "getcoord"={
 *              "method"= "GET",
 *              "path"="/event/{id}/coordonnees",
 *              "normalization_context" = {"groups"={"eventcoord:read"}},
 *              "formats"={"json"},
 *              "filters" = {},
 *              "pagination_enabled" = false,
 *              "openapi_context"= {
 *                  "parameters" = {},
 *                  "summary" = "Permet de récuperer les coordonnées d'un evenement à partir de son id.",
 *                  "description" = ""
 *              },
 *          }
 *      },
 *  collectionOperations={
 *      "get"={
 *         "formats"= {"json"},
 *         "filters" = {},
 *          "pagination_enabled" = false,
 *          "openapi_context"= {
 *              "parameters" = {},
 *              "summary" = "Permet de récuperer tous les évenements.",
 *              "description" = ""
 *          },
 *      },
 *      "getheavy"={
 *          "method"= "GET",
 *          "path"="/events/heavy",
 *          "formats"= {"json"},
 *          "normalization_context" = {"groups"={"eventheavy:read","eventcoord:read"}},
 *          "filters" = {},
 *          "pagination_enabled" = false,
 *          "openapi_context"= {
 *              "parameters" = {},
 *              "summary" = "Permet de récuprer tous les evenements avec leur détail.",
 *              "description" = ""
 *          },
 *      },
 *      "post"={
 *          "formats"= {"json"},
 *          "denormalization_context" = {"groups"={"newevent:write"}},
 *          "filters" = {},
 *          "pagination_enabled" = false,
 *          "openapi_context"= {
 *              "parameters" = {},
 *              "summary" = "Permet d'ajouter un évenement.",
 *              "description" = ""
 *          },
 *      },
 *      "getEventByKeywords"={
 *          "method"="GET",
 *          "path"="/events/getWithKey",
 *          "pagination_enabled"=false,
 *          "openapi_context" = {
 *              "summary" = "Recupere les events selon mot cle",
 *              "parameters"  = {
 *                 {
 *                     "in" = "query",
 *                     "name" = "keywords",
 *                     "schema" = {
 *                         "type" = "string"
 *                     },
 *                     "description" = "Filtre les evenements selon un mot cle"
 *                 }
 *             },
 *          },
 *          "controller"=GetEventWithKeywordsController::class,
 *      },
 *      "getEventsByDate"={
 *          "method"="GET",
 *          "path"="/events/getWithDate",
 *          "pagination_enabled"=false,
 *          "openapi_context" = {
 *              "summary" = "Recupere les events selon date",
 *              "parameters"  = {
 *                 {
 *                     "in" = "query",
 *                     "name" = "date",
 *                     "schema" = {
 *                         "type" = "string"
 *                     },
 *                     "description" = "Filtre les evenements selon date"
 *                 }
 *             },
 *          },
 *          "controller"=GetEventsWithDate::class,
 *      },
 *      "getEventsByLocalisation"={
 *          "method"="GET",
 *          "path"="/events/getWithLocalisation",
 *          "pagination_enabled"=false,
 *          "openapi_context" = {
 *              "summary" = "Recupere les events selon localisation",
 *              "parameters"  = {
 *                 {
 *                     "in" = "query",
 *                     "name" = "localisation",
 *                     "schema" = {
 *                         "type" = "string"
 *                     },
 *                     "description" = "Filtre les evenements selon localisation"
 *                 }
 *             },
 *          },
 *          "controller"=GetEventsWithLocalisation::class,
 *      },
 *  }
 * )
 * @ORM\Entity(repositoryClass=EventRepository::class)
 * 
 */

class Event
{

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"heavy:read","lazy:read", "eventsheavy:read","eventheavy:read","coord:read"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"heavy:read", "eventsheavy:read", "newevent:write","eventheavy:read","all:read"})
     */
    private $title;

    /**
     * @ORM\Column(type="datetime")
     * @Groups({"heavy:read", "eventsheavy:read", "newevent:write","eventheavy:read","all:read"})
     */
    private $date;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Groups({"heavy:read", "eventsheavy:read", "newevent:write","eventheavy:read","all:read"})
     */
    private $quota;

    /**
     * @ORM\Column(type="integer")
     * @Groups({"heavy:read", "eventsheavy:read", "newevent:write","eventheavy:read","all:read"})
     */
    private $cost;

    /**
     * @ORM\Column(type="text")
     * @Groups({"heavy:read", "eventsheavy:read", "newevent:write","eventheavy:read","all:read"})
     */
    private $description;

    /**
     * @ORM\Column(type="text")
     * @Groups({"heavy:read", "eventsheavy:read", "newevent:write","eventheavy:read","all:read"})
     */
    private $image;

    /**
     * @ORM\ManyToMany(targetEntity=User::class, mappedBy="events")
     * @Groups({"eventsheavy:read", "newevent:write","eventheavy:read","all:read"})
     */
    private $participants;

    /**
     * @ORM\ManyToOne(targetEntity=Coordinates::class, inversedBy="event", cascade={"persist"})
     * @Groups({"eventsheavy:read", "newevent:write","eventcoord:read"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $coordinates;

    /**
     * @ORM\ManyToOne(targetEntity=Association::class, inversedBy="events")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"newevent:write","eventheavy:read","all:read"})
     */
    private $association;

    public function __construct()
    {
        $this->participants = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getQuota(): ?int
    {
        return $this->quota;
    }

    public function setQuota(?int $quota): self
    {
        $this->quota = $quota;

        return $this;
    }

    public function getCost(): ?int
    {
        return $this->cost;
    }

    public function setCost(int $cost): self
    {
        $this->cost = $cost;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(string $image): self
    {
        $this->image = $image;

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getParticipants(): Collection
    {
        return $this->participants;
    }

    public function addParticipant(User $participant): self
    {
        if (!$this->participants->contains($participant)) {
            $this->participants[] = $participant;
            $participant->addEvent($this);
        }

        return $this;
    }

    public function removeParticipant(User $participant): self
    {
        if ($this->participants->removeElement($participant)) {
            $participant->removeEvent($this);
        }

        return $this;
    }

    public function getCoordinates(): ?Coordinates
    {
        return $this->coordinates;
    }

    public function setCoordinates(?Coordinates $coordinates): self
    {
        $this->coordinates = $coordinates;

        return $this;
    }

    public function getAssociation(): ?Association
    {
        return $this->association;
    }

    public function setAssociation(?Association $association): self
    {
        $this->association = $association;

        return $this;
    }
}
