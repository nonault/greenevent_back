<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Doctrine\Common\Annotations;
use Symfony\Component\Serializer\Annotation\Groups;
use App\Controller\BuyItemController;
use App\Controller\GetUserByEmailController;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use ApiPlatform\Core\Annotation\ApiProperty;



/**
 * @ApiResource(
 *      itemOperations= {
 *          "get",
 *          "put",
 *          
 *          "profile" = {
 *              "method" = "GET",
 *              "path" = "/users/{id}/profile",
 *              "normalization_context" = {"groups"={"profile:read"}},
 *              "pagination_enabled" = false,
 *              "openapi_context"= {
 *                  "parameters" = {},
 *                  "summary" = "Permet de récupérer un user avec son id.",
 *                  "description" = ""
 *              },
 *          },
 *          "asso" = {
 *              "method" = "PUT",
 *              "path" = "/user/{id}/association",
 *              "denormalization_context" = {"groups"={"asso:write"}},
 *              "formats"= {"json"},
 *              "pagination_enabled" = false,
 *              "openapi_context"= {
 *                  "parameters" = {},
 *                  "summary" = "Permet de devenir admin d'une association.",
 *                  "description" = ""
 *              },
 *          },
 *          "registrationevent" = {
 *              "method" = "PUT",
 *              "path" = "/user/{id}/register/event",
 *              "denormalization_context" = {"groups"={"event:write"}},
 *              "formats"= {"json"},
 *              "pagination_enabled" = false,
 *              "openapi_context"= {
 *                  "parameters" = {},
 *                  "summary" = "Permet de récupérer s'inscrire à un evenement.",
 *                  "description" = ""
 *              },
 *          },
 *          "likepost" = {
 *              "method" = "PUT",
 *              "path" = "/user/{id}/like/post",
 *              "denormalization_context" = {"groups"={"post:write"}},
 *              "formats"= {"json"},
 *              "pagination_enabled" = false,
 *              "openapi_context"= {
 *                  "parameters" = {},
 *                  "summary" = "Permet de récupérer liker un post.",
 *                  "description" = ""
 *              },
 *          },
 *          "buyitem" = {
 *              "method" = "PUT",
 *              "path" = "/user/{id}/buy/item",
 *              "controller" = BuyItemController::class,
 *              "denormalization_context" = {"groups"={"item:write"}},
 *              "formats"= {"json"},
 *              "write"=false,
 *              "validate"=false,
 *              "pagination_enabled" = false,
 *              "openapi_context"= {
 *                  "parameters" = {},
 *                  "summary" = "Permet de récupérer d'acheter un item avec ses points.",
 *                  "description" = ""
 *              },
 *              
 *          },
 *          "delete" = {
 *             "formats"= {"json"},
 *              "pagination_enabled" = false,
 *              "openapi_context"= {
 *                  "parameters" = {},
 *                  "summary" = "Permet de supprimer un utilisateur à partir de son id.",
 *                  "description" = ""
 *              },
 *          }
 *      },
 *      collectionOperations={
 *          "post" = {
 *              "formats"= {"json"},
 *              "pagination_enabled" = false,
 *              "openapi_context"= {
 *                  "parameters" = {},
 *                  "summary" = "Permet de récupérer créer un nouvel utilisateur.",
 *                  "description" = ""
 *              },
 *          },
 *          "login" =  {
 *              "method" = "POST",
 *              "path" = "/login",
 *              "controller" = SecurityController::class,
 *              "pagination_enabled" = false,
 *              "openapi_context"= {
 *                  "parameters" = {},
 *                  "summary" = "Permet de se logger.",
 *                  "description" = ""
 *              },
 *          },
 *          "getUserByEmail"={
    *          "method"="GET",
    *          "path"="/user/getEmail",
    *          "pagination_enabled"=false,
    *          "openapi_context" = {
    *              "summary" = "Recupere l'utilisateur selon email'",
    *              "parameters"  = {
    *                 {
    *                     "in" = "query",
    *                     "name" = "email",
    *                     "schema" = {
    *                         "type" = "string"
    *                     },
    *                     "description" = "Filtre selon l'email"
    *                 }
    *             },
    *          },
    *          "controller"=GetUserByEmailController::class,
    *       },
 *      },
 * )
 * @ORM\Entity(repositoryClass=UserRepository::class)
 * @ORM\Table(name="user",uniqueConstraints={@ORM\UniqueConstraint(name="email_idx", columns={"email"})})
 * @method string getUserIdentifier()
 */

class User implements UserInterface, PasswordAuthenticatedUserInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"admins:read","lazy:read"},{"profile:read"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"admins:read"},{"profile:read"})
     */
    private $lastname;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"admins:read"},{"profile:read"})
     */
    private $firstname;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"admins:read"},{"profile:read"})
     */
    private $email;

    /**
     * @ORM\Column(type="integer")
     * @Groups({"admins:read"},{"profile:read"})
     */
    private $points;

    /**
     * @ORM\ManyToMany(targetEntity=Event::class, inversedBy="participants")
     * @Groups({"event:write"})
     */
    private $events;

    /**
     * @ORM\ManyToOne(targetEntity=Association::class, inversedBy="admins")
     * @Groups({"asso:write"})
     */
    private $association;

    /**
     * @ORM\ManyToMany(targetEntity=Post::class, mappedBy="likes")
     * @Groups({"post:write"})
     */
    private $posts;

    /**
     * @ORM\ManyToMany(targetEntity=Item::class, inversedBy="users")
     * @ORM\JoinTable(name="user_item")
     * @Groups({"item:write"})
     */
    private $items;

    public function __construct()
    {
        $this->points = 0;
        $this->events = new ArrayCollection();
        $this->posts = new ArrayCollection();
        $this->items = new ArrayCollection();
        $this->itemss = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLastName(): ?string
    {
        return $this->lastname;
    }

    public function setLastName(string $lastname): self
    {
        $this->lastname = $lastname;

        return $this;
    }

    public function getFirstName(): ?string
    {
        return $this->firstname;
    }

    public function setFirstName(string $firstname): self
    {
        $this->firstname = $firstname;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getPoints(): ?int
    {
        return $this->points;
    }

    public function setPoints(int $points): self
    {
        $this->points = $points;

        return $this;
    }


    /**
     * @return Collection|Event[]
     */
    public function getEvents(): Collection
    {
        return $this->events;
    }

    public function addEvent(Event $event): self
    {
        if (!$this->events->contains($event)) {
            $this->events[] = $event;
        }

        return $this;
    }

    public function removeEvent(Event $event): self
    {
        $this->events->removeElement($event);

        return $this;
    }

    public function getAssociation(): ?Association
    {
        return $this->association;
    }

    public function setAssociation(?Association $association): self
    {
        $this->association = $association;

        return $this;
    }

    /**
     * @return Collection|Post[]
     */
    public function getPosts(): Collection
    {
        return $this->posts;
    }

    public function addPost(Post $post): self
    {
        if (!$this->posts->contains($post)) {
            $this->posts[] = $post;
            $post->addLike($this);
        }

        return $this;
    }

    public function removePost(Post $post): self
    {
        if ($this->posts->removeElement($post)) {
            $post->removeLike($this);
        }

        return $this;
    }

    public function getRoles(): array
    {
        return ['ROLE_USER'];
    }

    public function getSalt()
    {
        // TODO: Implement getSalt() method.
    }

    public function eraseCredentials()
    {
        // TODO: Implement eraseCredentials() method.
    }

    public function getUsername()
    {
        return (string) $this->email;
    }

    public function __call($name, $arguments)
    {
        // TODO: Implement @method string getUserIdentifier()
    }

    /**
     * @return Collection|Item[]
     */
    public function getItems(): Collection
    {
        return $this->items;
    }

    public function addItem(Item $item): self
    {
      
        if (!$this->items->contains($item)) {
            

            $this->items[] = $item;
        }

        return $this;
    }

    public function removeItem(Item $item): self
    {
        $this->items->removeElement($item);

        return $this;
    }

    
  
  
}
