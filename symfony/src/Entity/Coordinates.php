<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\CoordinatesRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(
 *  itemOperations = {
 *      "get" = {
 *          "formats"= {"json"},
 *          "filters" = {},
 *          "pagination_enabled" = false,
 *          "openapi_context"= {
 *              "parameters" = {},
 *              "summary" = "Permet de récupérer toutes les données sans détail d'une coordonnée.",
 *              "description" = "Cette route vous permet de récupérer les données sans détail d'une coordonnée'. Les données suivantes sont renvoyées: 
 * id,
 * name,
 * siret,
 * email,
 * phoneNumber,
 * évenements (id) dans un tableau,
 * administrateurs (id) dans un tableau,
 * coordonnées (id),
 * items (id) dans un tableau."}
 *      },
 *      "put" = {
 *          "formats"= {"json"},
 *          "filters" = {},
 *          "pagination_enabled" = false,
 *          "openapi_context"= {
 *              "parameters" = {},
 *              "summary" = "Permet de modifier une coordonnée à partir de son id."
 *          },
 *      },
 *      "delete" = {
 *          "formats"= {"json"},
 *          "filters" = {},
 *          "pagination_enabled" = false,
 *          "openapi_context"= {
 *              "parameters" = {},
 *              "summary" = "Permet de supprimer une coordonnée à partir de son id."
 *          },
 *      },
 *  },
 *  collectionOperations={
 *      "get"= {
 *          "formats"= {"json"},
 *          "filters" = {},
 *          "pagination_enabled" = false,
 *          "normalization_context" = {"groups"={"coord:read"}},
 *          "openapi_context"= {
 *              "parameters" = {},
 *              "summary" = "Permet de recupérer toutes les coordonnées."
 *          },
 *      },
 *      "post"= {
 *          "formats"= {"json"},
 *          "filters" = {},
 *          "pagination_enabled" = false,
 *          "openapi_context"= {
 *              "parameters" = {},
 *              "summary" = "Permet d'ajouter une coordonnée."
 *          },
 *      },
 *
 *  },
 * 
 *
 * )
 * @ORM\Entity(repositoryClass=CoordinatesRepository::class)
 */

class Coordinates
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"heavy:read","lazy:read","coord:read"})
     */
    private $id;

    /**
     * @ORM\Column(type="float")
     * @Groups({"heavy:read", "newevent:write","createasso:write","eventcoord:read","coord:read"})
     */
    private $latitude;

    /**
     * @ORM\Column(type="float")
     * @Groups({"heavy:read", "newevent:write","createasso:write","eventcoord:read","coord:read"})
     */
    private $longitude;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"heavy:read", "newevent:write","createasso:write","eventcoord:read","coord:read"})
     */
    private $address;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"heavy:read", "newevent:write","createasso:write","eventcoord:read","coord:read"})
     */
    private $category;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"heavy:read", "newevent:write","createasso:write","eventcoord:read","coord:read"})
     */
    private $name;

    /**
     * @ORM\Column(type="integer")
     * @Groups({"heavy:read", "newevent:write","createasso:write","eventcoord:read","coord:read"})
     */
    private $postalCode;

    /**
     * @ORM\OneToMany(targetEntity=Event::class, mappedBy="coordinates")
     * @Groups({"coord:read"})
     */
    private $event;

    /**
     * @ORM\OneToMany(targetEntity=Association::class, mappedBy="coordinates")
     * @Groups({"coord:read"})
     */
    private $association;

    

    public function __construct()
    {
        $this->event = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLatitude(): ?float
    {
        return $this->latitude;
    }

    public function setLatitude(float $latitude): self
    {
        $this->latitude = $latitude;

        return $this;
    }

    public function getLongitude(): ?float
    {
        return $this->longitude;
    }

    public function setLongitude(float $longitude): self
    {
        $this->longitude = $longitude;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getCategory(): ?string
    {
        return $this->category;
    }

    public function setCategory(string $category): self
    {
        $this->category = $category;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPostalCode(): ?int
    {
        return $this->postalCode;
    }

    public function setPostalCode(int $postalCode): self
    {
        $this->postalCode = $postalCode;

        return $this;
    }

    /**
     * @return Collection|Event[]
     */
    public function getEvent(): Collection
    {
        return $this->event;
    }

    public function addEvent(Event $event): self
    {
        if (!$this->event->contains($event)) {
            $this->event[] = $event;
            $event->setCoordinates($this);
        }

        return $this;
    }

    public function removeEvent(Event $event): self
    {
        if ($this->event->removeElement($event)) {
            // set the owning side to null (unless already changed)
            if ($event->getCoordinates() === $this) {
                $event->setCoordinates(null);
            }
        }

        return $this;
    }

     /**
     * @return Collection|Association []
     */
    public function getAssociation(): Collection
    {
        return $this->association;
    }

    public function addAssociation(Association $association): self
    {
        if (!$this->association->contains($association)) {
            $this->association[] = $association;
            $association->setCoordinates($this);
        }

        return $this;
    }

    public function removeAssociation(Association $association): self
    {
        if ($this->association->removeElement($association)) {
            // set the owning side to null (unless already changed)
            if ($association->getCoordinates() === $this) {
                $association->setCoordinates(null);
            }
        }

        return $this;
    }
}
