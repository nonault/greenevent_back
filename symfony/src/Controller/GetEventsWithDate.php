<?php

namespace App\Controller;

use App\Entity\Event;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Psr\Log\LoggerInterface;



class GetEventsWithDate
{
    private $em;
    private $logger;

    public function __construct(EntityManagerInterface $em, LoggerInterface $logger)
    {
        $this->em = $em;
        $this->logger = $logger;
    }

    public function __invoke(Request $request)
    {

        $this->logger->info('Debug date parameters :');
        $this->logger->info($request->get('date'));
        $this->logger->info(gettype($request->get('date')));

        $dateQuery= $request->get('date');
        $dateFin = explode(" ", $dateQuery)[0] . " 23:59:59";

        $this->logger->info(gettype($dateQuery));
        
        return $this->em->getRepository(Event::class)->getEventsByDate($dateQuery, $dateFin);
        
    }
    
}