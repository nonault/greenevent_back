<?php

namespace App\Controller;

use App\Entity\Event;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;



class GetEventWithKeywordsController
{
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function __invoke(Request $request)
    {
        $keywordsQuery = $request->get('keywords');
        if(!empty($keywordsQuery)){
            return $this->em->getRepository(Event::class)->getEventsByKeywords($keywordsQuery);
        }else{
            return $this->em->getRepository(Event::class)->getNothing();
        }

    }
    
}