<?php

namespace App\Controller;

use App\Entity\User;
use App\Exception\NoPointsException;
use App\Exception\NotEnoughtItemException;
use App\Repository\ItemRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;


class BuyItemController {
    private $emItem;
    private $manager;

    public function __construct(
        ItemRepository $emItem,
        EntityManagerInterface $manager
    ) {
        $this->emItem = $emItem;
        $this->manager = $manager;
    }

    public function __invoke(User $data, Request $request)
    {
      
        $item_id = json_decode($request->getContent())->users[0];
        if(preg_match("/\/(\d+)$/",$item_id,$matches))
        {
            $id=$matches[1];
        }
        
        $item = $this->emItem->find($id);
        $cost = $item->getQuantity();
        $points = $data->getPoints();
        echo $points;
        echo $cost;
        if ($points<$cost){
            throw new NoPointsException(sprintf("L'utilisateur %s n'a pas assez de points pour acheter l'item", $data->getId()));
        }
        if ($item->getQuantity()<=0){
            throw new NotEnoughtItemException(sprintf("L'utilisateur %s ne peux pas acheter l'item, il n'y a plus de stock", $data->getId()));
        }
        $item->setQuantity($item->getQuantity()-1);
        
        $data->setPoints($data->getPoints()-$item->getCost());

        $this->manager->flush();

    }


}