<?php

namespace App\Controller;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;



class GetUserByEmailController
{
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function __invoke(Request $request)
    {
        $emailQuery = $request->get('email');
        if(!empty($emailQuery)){
            return $this->em->getRepository(User::class)->getUserByEmail($emailQuery);
        }
    }
    
}