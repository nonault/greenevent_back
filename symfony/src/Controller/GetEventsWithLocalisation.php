<?php

namespace App\Controller;

use App\Entity\Event;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Psr\Log\LoggerInterface;



class GetEventsWithLocalisation
{
    private $em;
    private $logger;

    public function __construct(EntityManagerInterface $em, LoggerInterface $logger)
    {
        $this->em = $em;
        $this->logger = $logger;
    }

    public function __invoke(Request $request)
    {
        $localisationQuery = $request->get('localisation');
        return $this->em->getRepository(Event::class)->getEventsByLocalisation($localisationQuery);
       //echo $this->em;

    }
    
}