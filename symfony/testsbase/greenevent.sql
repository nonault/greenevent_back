-- import to SQLite by running: sqlite3.exe db.sqlite3 -init sqlite.sql

PRAGMA journal_mode = MEMORY;
PRAGMA synchronous = OFF;
PRAGMA foreign_keys = OFF;
PRAGMA ignore_check_constraints = OFF;
PRAGMA auto_vacuum = NONE;
PRAGMA secure_delete = OFF;
BEGIN TRANSACTION;

CREATE TABLE `association` (
`id` INTEGER PRIMARY KEY,
`coordinates_id` INTEGER NOT NULL,
`name` TEXT  NOT NULL,
`siret` INTEGER NOT NULL UNIQUE,
`email` TEXT  NOT NULL,
`phone_number` TEXT  NOT NULL,
`image` TEXT,
`description` TEXT,
FOREIGN KEY (`coordinates_id`) REFERENCES `coordinates` (`id`)
);

CREATE TABLE `coordinates` (
`id` INTEGER PRIMARY KEY,
`latitude` double NOT NULL,
`longitude` double NOT NULL,
`address` TEXT  NOT NULL,
`category` TEXT  NOT NULL,
`name` TEXT  NOT NULL,
`postal_code` INTEGER NOT NULL
);


CREATE TABLE `doctrine_migration_versions` (
`version` TEXT  NOT NULL,
`executed_at` datetime DEFAULT NULL,
`execution_time` INTEGER DEFAULT NULL
);

CREATE TABLE `event` (
`id` INTEGER PRIMARY KEY,
`title` TEXT  NOT NULL,
`date` datetime NOT NULL,
`quota` INTEGER DEFAULT NULL,
`cost` INTEGER NOT NULL,
`description` TEXT  NOT NULL,
`image` TEXT,
`coordinates_id` INTEGER NOT NULL,
`association_id` INTEGER NOT NULL,
FOREIGN KEY (`coordinates_id`) REFERENCES `coordinates` (`id`),
FOREIGN KEY (`association_id`) REFERENCES `association` (`id`)
);

CREATE TABLE `item` (
`id` INTEGER PRIMARY KEY,
`name` TEXT  NOT NULL,
`quantity` INTEGER NOT NULL,
`cost` INTEGER NOT NULL,
`association_id` INTEGER NOT NULL,
FOREIGN KEY (`association_id`) REFERENCES `association` (`id`)
);

CREATE TABLE `post` (
`id` INTEGER PRIMARY KEY,
`title` TEXT  NOT NULL,
`description` TEXT  NOT NULL,
`contenu` TEXT  NOT NULL,
`association_id` INTEGER NOT NULL,
FOREIGN KEY (`association_id`) REFERENCES `association` (`id`)
);

CREATE TABLE `post_user` (
`post_id` INTEGER NOT NULL,
`user_id` INTEGER NOT NULL,
FOREIGN KEY (`post_id`) REFERENCES `post` (`id`) ON DELETE CASCADE,
FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE
);

CREATE TABLE `user` (
`id` INTEGER PRIMARY KEY,
`password` TEXT  NOT NULL,
`email` TEXT  NOT NULL UNIQUE,
`points` INTEGER NOT NULL,
`association_id` INTEGER DEFAULT NULL,
`lastname` TEXT  NOT NULL,
`firstname` TEXT  NOT NULL,
FOREIGN KEY (`association_id`) REFERENCES `association` (`id`)
);

CREATE TABLE `user_event` (
`user_id` INTEGER NOT NULL,
`event_id` INTEGER NOT NULL,
FOREIGN KEY (`event_id`) REFERENCES `event` (`id`) ON DELETE CASCADE,
FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE
);

CREATE TABLE `user_item` (
`user_id` INTEGER NOT NULL,
`item_id` INTEGER NOT NULL,
FOREIGN KEY (`item_id`) REFERENCES `item` (`id`) ON DELETE CASCADE,
FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE
);

INSERT INTO `user_item` (`user_id`, `item_id`) VALUES
(1, 1),
(1, 3),
(3, 1);









COMMIT;
PRAGMA ignore_check_constraints = ON;
PRAGMA foreign_keys = ON;
PRAGMA journal_mode = WAL;
PRAGMA synchronous = NORMAL;
