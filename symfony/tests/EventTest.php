<?php

namespace App\tests;

use App\Entity\Event;
use PHPUnit\Framework\TestCase;

class EventTest extends TestCase
{
    private Event $event;

    protected function setUp(): void
    {
        parent::setUp();
        $this->event = new Event();
    }

    public function testGetTitle(): void
    {
        $value = 'testTitle';

        $response = $this->event->setTitle($value);
        self::assertEquals($value,$this->event->getTitle());
    }
}