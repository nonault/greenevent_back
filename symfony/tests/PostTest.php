<?php

namespace App\tests;

use App\Entity\Post;
use PHPUnit\Framework\TestCase;

class PostTest extends TestCase
{
    private Post $post;

    protected function setUp(): void
    {
        parent::setUp();
        $this->post = new Post();
    }

    public function testGetTitle(): void
    {
        $value = 'testTitle';

        $response = $this->post->setTitle($value);
        self::assertEquals($value,$this->post->getTitle());
    }
}