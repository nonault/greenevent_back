<?php

namespace App\tests;

use App\Entity\Association;
use PHPUnit\Framework\TestCase;

class AssociationTest extends TestCase
{
    private Association $association;

    protected function setUp(): void
    {
        parent::setUp();
        $this->association = new Association();
    }

    public function testGetSiret(): void
    {
        $value = 57;

        $response = $this->association->setSiret($value);
        self::assertEquals($value,$this->association->getSiret());
    }
}