<?php

namespace App\tests;

use App\Entity\Item;
use PHPUnit\Framework\TestCase;

class ItemTest extends TestCase
{
    private Item $item;

    protected function setUp(): void
    {
        parent::setUp();
        $this->item = new Item();
    }

    public function testGetName(): void
    {
        $value = 'testItem';

        $response = $this->item->setName($value);
        self::assertEquals($value,$this->item->getName());
    }

    public function testGetQuantity(): void
    {
        $value = 5;

        $response = $this->item->setQuantity($value);
        self::assertEquals($value,$this->item->getQuantity());
    }
}