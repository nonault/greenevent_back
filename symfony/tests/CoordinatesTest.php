<?php

namespace App\tests;

use App\Entity\Coordinates;
use PHPUnit\Framework\TestCase;

class CoordinatesTest extends TestCase
{
    private Coordinates $coordinates;

    protected function setUp(): void
    {
        parent::setUp();
        $this->coordinates = new Coordinates();
    }

    public function testGetLatitude(): void
    {
        $value = 5;

        $response = $this->coordinates->setLatitude($value);
        self::assertEquals($value,$this->coordinates->getLatitude());
    }

    public function testGetLongitude(): void
    {
        $value = 5;

        $response = $this->coordinates->setLongitude($value);
        self::assertEquals($value,$this->coordinates->getLongitude());
    }

    public function testGetAddress(): void
    {
        $value = 'testAddresse';

        $response = $this->coordinates->setAddress($value);
        self::assertEquals($value,$this->coordinates->getAddress());
    }
}