<?php

namespace App\tests;

use App\Entity\User;
use PHPUnit\Framework\TestCase;

class UserTest extends TestCase
{
    private User $user;

    protected function setUp(): void
    {
        parent::setUp();
        $this->user = new User();
    }

    public function testGetMail(): void
    {
        $value = 'test@test.fr';

        $response = $this->user->setEmail($value);
        self::assertEquals($value,$this->user->getEmail());
        self::assertEquals($value,$this->user->getUsername());
    }

    public function testGetPoints(): void
    {
        $value = 0;

        $response = $this->user->setPoints($value);
        self::assertEquals($value,$this->user->gePoints());
    }
}