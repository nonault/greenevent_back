-- import to SQLite by running: sqlite3.exe db.sqlite3 -init sqlite.sql

PRAGMA journal_mode = MEMORY;
PRAGMA synchronous = OFF;
PRAGMA foreign_keys = OFF;
PRAGMA ignore_check_constraints = OFF;
PRAGMA auto_vacuum = NONE;
PRAGMA secure_delete = OFF;
BEGIN TRANSACTION;

CREATE TABLE `association` (
`id` INTEGER PRIMARY KEY,
`coordinates_id` INTEGER NOT NULL,
`name` TEXT  NOT NULL,
`siret` INTEGER NOT NULL UNIQUE,
`email` TEXT  NOT NULL,
`phone_number` TEXT  NOT NULL,
`image` TEXT,
`description` TEXT,
FOREIGN KEY (`coordinates_id`) REFERENCES `coordinates` (`id`)
);
INSERT INTO `association` (`id`, `coordinates_id`, `name`, `siret`, `email`, `phone_number`) VALUES
(10, 1, 'Bougeons', 19383, 'greenevent@gmail.com', '0678787575'),
(11, 2, 'GreenPlanet', 276374, 'greenplanet@gmail.com', '0678787575'),
(12, 3, 'MaTerre', 9876545678, 'materre@gmail.com', '0678787575'),
(13, 4, 'SunAction', 876543567, 'sunaction@gmail.com', '0678787575'),
(14, 5, 'EcoLand', 45678654, 'ecoland@gmail.com', '0678787575'),
(15, 6, 'Ecologica', 43456789, 'ecologica@gmail.com', '0678787575'),
(16, 7, 'Plantation', 765432356789, 'plantation@gmail.com', '0678787575');

CREATE TABLE `coordinates` (
`id` INTEGER PRIMARY KEY,
`latitude` double NOT NULL,
`longitude` double NOT NULL,
`address` TEXT  NOT NULL,
`category` TEXT  NOT NULL,
`name` TEXT  NOT NULL,
`postal_code` INTEGER NOT NULL
);
INSERT INTO `coordinates` (`id`, `latitude`, `longitude`, `address`, `category`, `name`, `postal_code`) VALUES
(1, 37.4, 2.3, '30 rue de la marne', 'association', 'Bougeons', 29303),
(2, -2.3, 2.3, '10 rue de toto', 'association', 'GreenPlanet', 30994),
(3, 1.2, 2.9, '1 allée des hérons', 'association', 'MaTerre', 35463),
(4, 7.9, 2.3, '2 bis route de la ville', 'association', 'SunAction', 18736),
(5, -32.3, 2.3, '32 allée des coquelicots', 'association', 'Ecoland', 09827),
(6, 2.3, 4.5, '9 rue de la paix', 'association', 'Ecologica', 87367),
(7, 4.6, -2.3, '2 rue des lys', 'association', 'Plantation', 27640),
(9, 4.6, -2.3, '2 rue des lys', 'evenement', 'EcoJogging', 27640),
(10, 4.6, -2.3, '2 rue des lys', 'evenement', 'Ramassage', 27640),
(11, 4.6, -2.3, '2 rue des lys', 'evenement', 'DIY lessive', 27640),
(12, 4.6, -2.3, '2 rue des lys', 'evenement', 'Poubelles', 27640),
(13, 4.6, -2.3, '2 rue des lys', 'evenement', 'Collecte', 27640),
(8, 10.5, 9.8, '26 route de la maison', 'evenement', 'DIY bocaux', 09834);

CREATE TABLE `doctrine_migration_versions` (
`version` TEXT  NOT NULL,
`executed_at` datetime DEFAULT NULL,
`execution_time` INTEGER DEFAULT NULL
);

CREATE TABLE `event` (
`id` INTEGER PRIMARY KEY,
`title` TEXT  NOT NULL,
`date` datetime NOT NULL,
`quota` INTEGER DEFAULT NULL,
`cost` INTEGER NOT NULL,
`description` TEXT  NOT NULL,
`image` TEXT,
`coordinates_id` INTEGER NOT NULL,
`association_id` INTEGER NOT NULL,
FOREIGN KEY (`coordinates_id`) REFERENCES `coordinates` (`id`),
FOREIGN KEY (`association_id`) REFERENCES `association` (`id`)
);
INSERT INTO `event` (`id`, `title`, `date`, `quota`, `cost`, `description`, `coordinates_id`, `association_id`) VALUES
(1, 'EcoJogging', '2021-11-09 21:12:11', 10, 0, 'Course de ramassage de déchets', 8, 10),
(2, 'Ramassage', '2021-11-09 21:12:11', 9, 0, 'Ramassage de déchets dans le centre ville', 9, 10),
(3, 'DIY lessive', '2021-11-09 21:12:11', 30, 30, 'Nous vous convions à un atelier DIY de lessive.', 10, 10),
(4, 'Poubelles', '2021-11-09 21:12:11', 56, 0, 'Ramassage des poubelles', 11, 10),
(6, 'Collecte', '2021-11-09 21:12:11', 100, 0, 'Venez nous rencontrer afin de nous faire don de vos vêtements usagés', 12, 10),
(5, 'DIY bocaux', '2021-11-09 21:19:21', 23, 50, 'Nous vous convions à un atelier DIY de bocaux.', 13, 10);

CREATE TABLE `item` (
`id` INTEGER PRIMARY KEY,
`name` TEXT  NOT NULL,
`quantity` INTEGER NOT NULL,
`cost` INTEGER NOT NULL,
`association_id` INTEGER NOT NULL,
FOREIGN KEY (`association_id`) REFERENCES `association` (`id`)
);

CREATE TABLE `post` (
`id` INTEGER PRIMARY KEY,
`title` TEXT  NOT NULL,
`description` TEXT  NOT NULL,
`contenu` TEXT  NOT NULL,
`association_id` INTEGER NOT NULL,
FOREIGN KEY (`association_id`) REFERENCES `association` (`id`)
);

CREATE TABLE `post_user` (
`post_id` INTEGER NOT NULL,
`user_id` INTEGER NOT NULL,
FOREIGN KEY (`post_id`) REFERENCES `post` (`id`) ON DELETE CASCADE,
FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE
);

CREATE TABLE `user` (
`id` INTEGER PRIMARY KEY,
`password` TEXT  NOT NULL,
`email` TEXT  NOT NULL UNIQUE,
`points` INTEGER NOT NULL,
`association_id` INTEGER DEFAULT NULL,
`lastname` TEXT  NOT NULL,
`firstname` TEXT  NOT NULL,
FOREIGN KEY (`association_id`) REFERENCES `association` (`id`)
);
INSERT INTO `user` (`id`, `password`, `email`, `points`, `association_id`, `lastname`, `firstname`) VALUES
(9, '$2y$13$f8DOHyp6F81ktYlBf2swb.tFq17Y0FMLI7VYv/SW1nKCQ0Ygt885S', 'test', 0, NULL, 'test', 'test');

CREATE TABLE `user_event` (
`user_id` INTEGER NOT NULL,
`event_id` INTEGER NOT NULL,
FOREIGN KEY (`event_id`) REFERENCES `event` (`id`) ON DELETE CASCADE,
FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE
);

CREATE TABLE `user_item` (
`user_id` INTEGER NOT NULL,
`item_id` INTEGER NOT NULL,
FOREIGN KEY (`item_id`) REFERENCES `item` (`id`) ON DELETE CASCADE,
FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE
);

INSERT INTO `user_item` (`user_id`, `item_id`) VALUES
(1, 1),
(1, 3),
(3, 1);









COMMIT;
PRAGMA ignore_check_constraints = ON;
PRAGMA foreign_keys = ON;
PRAGMA journal_mode = WAL;
PRAGMA synchronous = NORMAL;
